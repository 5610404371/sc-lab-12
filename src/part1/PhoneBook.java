package part1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class PhoneBook {

	public void readFile() {
		String filename = "phone book.txt";
		FileReader filereader = null;
		try {
			filereader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(filereader);

			String line = buffer.readLine();

			System.out.println("--------- Java Phone Book ---------");
			System.out.println("Name" + "\t" + "Phone" + "\n" + "====" + "\t"
					+ "=====");

			while (line != null) {
				System.out.println(line);
				line = buffer.readLine();
			}

		} catch (FileNotFoundException e) {
			System.err.print("File not found!");
		} catch (IOException e) {
			System.err.print("Error reading from file.");
			e.printStackTrace();
		} finally{
			try {
				 if (filereader != null) filereader.close();
				 
			 } catch (IOException e) {
				 System.err.println("Error closing files");
			 }
		}
	}

	public void writeFile() {
		String filename = "Phone book.txt";
		FileWriter filewriter = null;
		try {
			InputStreamReader inReader = new InputStreamReader(System.in);
			BufferedReader buffer = new BufferedReader(inReader);

			filewriter = new FileWriter(filename, true);
			PrintWriter out = new PrintWriter(new BufferedWriter(filewriter));

			String line = buffer.readLine();
			while (!line.equals("end")) {
				out.println(line);
				line = buffer.readLine();
			}
			out.flush();

		} catch (FileNotFoundException e) {
			System.err.print("File not found!");
		} catch (IOException e) {
			System.err.print("Error reading from file.");
			e.printStackTrace();
		}finally{
			try {
				 if (filewriter != null) filewriter.close();
				 
			 } catch (IOException e) {
				 System.err.println("Error closing files");
			 }
		}
	}

}
