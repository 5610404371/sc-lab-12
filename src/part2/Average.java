package part2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Average {

	public void readandwriteFile() {
		String filename = "homework.txt";
		String filename1 = "average.txt";
		FileReader reader = null;
		FileWriter writer = null;
		try {
			reader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(reader);
			
			writer = new FileWriter(filename1);
			PrintWriter write = new PrintWriter(new BufferedWriter(writer));

			System.out.println("--------- Homework Scores ---------");
			System.out.println("Name" + "\t" + "Average" + "\n" + "======="
					+ "\t" + "=====");

			String line = buffer.readLine();

			while (line != null) {
				String[] s = line.split(", ");
				int a = s.length;
				float averageScore = 0;
				for (int i = 1; i < s.length; i++) {
					averageScore += Float.parseFloat(s[i]);
				}
				averageScore = averageScore / (a - 1);
				write.println(s[0]+", "+averageScore);
				System.out.println(s[0] + "\t" + averageScore);
				line = buffer.readLine();
			}
			write.flush();
			
		} catch (FileNotFoundException e) {
			System.err.print("ERROR : File Not Found.");
		} catch (IOException e) {
			System.err.print("Error reading from file");
		} finally{
			try {
				 if (reader != null) reader.close();
				 
			 } catch (IOException e) {
				 System.err.println("Error closing files");
			 }
		}

	}

}
