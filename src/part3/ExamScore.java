package part3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class ExamScore {

	public void readFile() {
		String filename = "exam.txt";
		String filename1 = "average.txt";
		FileReader reader = null;
		FileReader reader1 = null;
		FileWriter writer = null;
		BufferedReader buffer;
		try {
			reader1 = new FileReader(filename1);
			buffer = new BufferedReader(reader1);

			System.out.println("--------- Homework Scores ---------");
			System.out.println("Name" + "\t" + "Average" + "\n" + "======="
					+ "\t" + "=====");

			String line1 = buffer.readLine();
			while (line1 != null) {
				System.out.println(line1);
				line1 = buffer.readLine();
			}
			reader = new FileReader(filename);
			buffer = new BufferedReader(reader);

			writer = new FileWriter("average.txt", true);
			PrintWriter out = new PrintWriter(new BufferedWriter(writer));

			System.out.println("--------- Exam Scores ---------");
			System.out.println("Name" + "\t" + "New Average" + "\n" + "======="
					+ "\t" + "=====");

			String line = buffer.readLine();
			while (line != null) {
				String[] s = line.split(", ");
				int a = s.length;
				float averageScore = 0;
				for (int i = 1; i < s.length; i++) {
					averageScore += Float.parseFloat(s[i]);
				}
				averageScore = averageScore / (a - 1);
				System.out.println(s[0] + "\t"+averageScore);
				out.println(s[0] + ", " + averageScore);
				line = buffer.readLine();
			}
			out.flush();

		} catch (FileNotFoundException e) {
			System.err.print("ERROR : File Not Found.");
		} catch (IOException e) {
			System.err.print("Error reading from file");
		}
	}

}
